FROM ubuntu:20.04

LABEL Description="KDE Static website generation image"
MAINTAINER Bhushan Shah

USER root

ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

# TODO: See if this can be run with -–no-install-recommends without anything breaking
# since that would reduce the number of fetched packages from 350 to 280
RUN apt-get update && apt-get install --yes \
ruby \
ruby-dev \
build-essential \
libsqlite3-dev \
zlib1g-dev \
git git-lfs \
python3-pip \
python3-venv \
php7.4-cli php7.4-xml \
fontforge \
locales \
golang-go \
doxygen graphviz qdoc-qt5 \
wget curl rsync pandoc subversion gettext && apt-get clean

RUN locale-gen en_US.UTF-8

RUN curl -sS https://deb.nodesource.com/gpgkey/nodesource.gpg.key | apt-key add - && echo "deb https://deb.nodesource.com/node_14.x focal main" > /etc/apt/sources.list.d/nodesource.list
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install --yes yarn nodejs && apt-get clean

RUN wget https://github.com/gohugoio/hugo/releases/download/v0.94.2/hugo_extended_0.94.2_Linux-64bit.deb && \
	dpkg -i hugo_extended_0.94.2_Linux-64bit.deb && rm hugo_extended_0.94.2_Linux-64bit.deb

RUN gem update --system && gem install xdg:2.2.5 jekyll:3.8.4 jekyll-feed jekyll-theme-minimal jekyll-readme-index jekyll-relative-links jekyll-environment-variables pluto bundler haml redcarpet minima rdiscount inqlude addressable jekyll-kde-theme rouge

RUN pip3 install sphinx-intl aether-sphinx sphinx_rtd_theme sphinxcontrib-doxylink sphinxcontrib-websupport requests pytx dateparser pelican mdx_video markdown beautifulsoup4 polib python-frontmatter jieba py-appstream pyyaml python-gitlab paramiko

RUN wget https://getcomposer.org/download/1.9.0/composer.phar && mv composer.phar /usr/bin/composer && chmod +x /usr/bin/composer

RUN apt-get -qq update && apt-get install -qq \
    openssh-server \
    openjdk-8-jre-headless \
    && dpkg-reconfigure openssh-server && mkdir -p /var/run/sshd && apt-get -qq clean

RUN git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n

# Generation of api docs for api.kde.org
RUN git clone https://invent.kde.org/frameworks/kapidox.git /kapidox \
    && pip3 install --no-deps -r /kapidox/requirements.frozen.txt /kapidox

# Setup a user account for everything else to be done under
RUN useradd -d /home/user/ -u 1000 --user-group --create-home -G video user

EXPOSE 22
CMD ["/usr/sbin/sshd", "-D"]
